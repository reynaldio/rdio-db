package pgx

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

type PgxConn interface {
	Begin(ctx context.Context) (pgx.Tx, error)
	BeginTx(ctx context.Context, txOptions pgx.TxOptions) (pgx.Tx, error)
}

type pgxConnPool struct {
	conn pgxpool.Conn
}

func (self *pgxConnPool) Begin(ctx context.Context) (pgx.Tx, error) {
	return self.Begin(ctx)
}

func (self *pgxConnPool) BeginTx(ctx context.Context, txOptions pgx.TxOptions) (pgx.Tx, error) {
	return self.BeginTx(ctx, txOptions)
}

type pgxConnClient struct {
	conn pgx.Conn
}

func (self *pgxConnClient) Begin(ctx context.Context) (pgx.Tx, error) {
	return self.Begin(ctx)
}

func (self *pgxConnClient) BeginTx(ctx context.Context, txOptions pgx.TxOptions) (pgx.Tx, error) {
	return self.BeginTx(ctx, txOptions)
}

func GetPgxConn[T pgxpool.Conn | pgx.Conn](conn T) (PgxConn, error) {
	switch any(conn).(type) {
	case pgxpool.Conn:
		c := any(conn).(pgxpool.Conn)
		instance := &pgxConnPool{conn: c}
		return instance, nil
	case pgx.Conn:
		c := any(conn).(pgxpool.Conn)
		instance := &pgxConnPool{conn: c}
		return instance, nil
	default:
		return nil, errors.New("Invalid pgx connection type")
	}
}
